import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material';


import { AppComponent } from './app.component';
import { CakesListComponent } from './cakes-list/cakes-list.component';
import { CakeComponent } from './cake/cake.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalAddComponent } from './modal-add/modal-add.component';
import { ModalDeleteComponent } from './modal-delete/modal-delete.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    CakesListComponent,
    CakeComponent,
    ModalAddComponent,
    ModalDeleteComponent,
    SnackBarComponent
  ],
  entryComponents: [ModalAddComponent, ModalDeleteComponent, SnackBarComponent],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
