import { Component, Inject, Output, EventEmitter, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalData } from '../modal-data';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-add',
  templateUrl: './modal-add.component.html',
  styleUrls: ['./modal-add.component.scss']
})
export class ModalAddComponent {

  linkRegex = 'https?://.+';
  nameFormControl = new FormControl('', [Validators.required]);
  // descriptionFormControl = new FormControl('', [Validators.maxLength(250)]);
  partsFormControl = new FormControl('', [Validators.required]);
  priceFormControl = new FormControl('', [Validators.required]);
  photoFormControl = new FormControl('', [Validators.required, Validators.pattern(this.linkRegex)]);
  error = false;

  @Input() price;

  @Output() cakeCreated = new EventEmitter<ModalData>();
  description = this.data.description;

  constructor(
    public dialogRef: MatDialogRef<ModalAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  createCake() {
    if (this.data.description) {
      this.data.description = '';
    } else {
      this.cakeCreated.emit();

    }
  }
}
