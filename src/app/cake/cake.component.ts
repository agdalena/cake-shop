import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalData } from '../modal-data';
import { CakesListComponent } from '../cakes-list/cakes-list.component';

@Component({
  selector: 'app-cake',
  templateUrl: './cake.component.html',
  styleUrls: ['./cake.component.scss']
})
export class CakeComponent implements OnInit {
  constructor(public cakelist: CakesListComponent) {}

  @Input()
  data: ModalData;

  @Output()
  cakeRemoved = new EventEmitter<ModalData>();
  @Output()
  cakeEdit = new EventEmitter<ModalData>();

  ngOnInit() {
  }

  delete(cake) {
    this.cakeRemoved.emit(cake);
  }

  edit(cake) {
    this.cakeEdit.emit(cake);
  }
}
