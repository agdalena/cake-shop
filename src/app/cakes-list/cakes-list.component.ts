import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { ModalAddComponent } from '../modal-add/modal-add.component';
import { ModalDeleteComponent } from '../modal-delete/modal-delete.component';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';

@Component({
  selector: 'app-cakes-list',
  templateUrl: './cakes-list.component.html',
  styleUrls: ['./cakes-list.component.scss']
})
export class CakesListComponent implements OnInit {
  name: string;
  description: string;
  parts: number;
  price: number;
  photo: string;
  cakes = new Array();
  empty: boolean;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar) {}

  ngOnInit() {
    const localCakes = JSON.parse(localStorage.getItem('dataCake'));
    this.cakes = localCakes;
    if (localCakes === null || localCakes.length === 0) {
      this.empty = true;
    }
    if (this.cakes === null) {
      this.cakes = [];
    }
  }

  openSnackBar(notify) {
    this.snackBar.openFromComponent(SnackBarComponent, {
      data: notify,
      duration: 5000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      panelClass: 'snack-notify'
    });
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(ModalAddComponent, {
      panelClass: 'cakeshop-dialog',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(this.cakes);
      this.cakes.push(result);
      localStorage.setItem('dataCake', JSON.stringify(this.cakes));
      this.empty = false;
      this.openSnackBar('DODAŁEŚ NOWE CIASTO!');
    });
  }

  editDialog(cake): void {
    const indexToDelete = this.cakes.indexOf(cake);
    let cakeLocal;
    if (indexToDelete !== -1) {
      cakeLocal = this.cakes.splice(indexToDelete, 1);
    }
    const dialogRef = this.dialog.open(ModalAddComponent, {
      panelClass: 'cakeshop-dialog',
      data: {
        name: cakeLocal[0].name,
        photo: cakeLocal[0].photo,
        description: cakeLocal[0].description,
        parts: cakeLocal[0].parts,
        price: cakeLocal[0].price
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.cakes.push(result);
      localStorage.setItem('dataCake', JSON.stringify(this.cakes));
      this.empty = false;
      this.openSnackBar('UDAŁO SIE EDYTOWAĆ!');
    });
  }

  public deleteDialog(cake): void {
    const dialogRef = this.dialog.open(ModalDeleteComponent, {
      panelClass: 'cakeshop-dialog-delete',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (JSON.stringify(result) === 'true') {
        const indexToDelete = this.cakes.indexOf(cake);
        if (indexToDelete !== -1) {
          this.cakes.splice(indexToDelete, 1);
          localStorage.removeItem('dataCake');
          localStorage.setItem('dataCake', JSON.stringify(this.cakes));
          this.openSnackBar('USUNĄŁEŚ CIASTO!');
        }
        if (this.cakes.length === 0) {
          this.empty = true;
        }
      }
    });
  }
}
