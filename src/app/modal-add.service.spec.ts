import { TestBed, inject } from '@angular/core/testing';

import { ModalAddService } from './modal-add.service';

describe('ModalAddService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalAddService]
    });
  });

  it('should be created', inject([ModalAddService], (service: ModalAddService) => {
    expect(service).toBeTruthy();
  }));
});
