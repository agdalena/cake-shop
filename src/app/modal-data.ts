export interface ModalData {
  photo: string;
  name: string;
  description: string;
  parts: number;
  price: number;
}
